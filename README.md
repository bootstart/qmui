# qmui

#### 介绍
qmui 是腾讯Android UI  框架, 此项目是为了方面初学者和专业开发人员快速创建 基于qmui的项目为目标.

#### 软件架构
软件架构说明


#### 安装教程

1.  浏览git说明
2.  克隆到本地

#### 使用说明

1.  使用 android studio 导入本项目
2.  编译后运行即可查看效果
3.  修改 activity_main.xml 实现自己的显示内容
4.  该项目仅  使用 QMUI.Compat.NoActionBar, 和QMUI.Compat的区别是 app运行时不显示 app name 标题栏 ,由于AndroidManifest.xml 文件中
         "android:theme="@style/AppTheme"  是在style.xml 中定义, 内容为:
		 <style name="AppTheme" parent="QMUI.Compat.NoActionBar">
        <!-- Customize your theme here. -->
        <item name="colorPrimary">@color/colorPrimary</item>
        <item name="colorPrimaryDark">@color/colorPrimaryDark</item>
        <item name="colorAccent">@color/colorAccent</item>
        </style>
		所以 "android:theme="@style/AppTheme" 是继承自 QMUI.Compat.NoActionBar.  因此全局 使用了 QMUI.Compat.NoActionBar.
	
5.  xml中引用 qmui中定义的资源,可以参考 app:qmui_backgroundColor="?attr/qmui_config_color_separator" , 这里
         qmui_config_color_separator是在qmui源码中定义,具体位置如下:
         [qmui/src/main/res/values/qmui_colors.xml](https://github.com/Tencent/QMUI_Android/blob/b6993d99de35ce20de48e67332856cfec5ff3cb0/qmui/src/main/res/values/qmui_colors.xml)
         
				 

```xml

			<color name="qmui_config_color_separator">#DEE0E2</color>
			<color name="qmui_config_color_separator_darken">#D4D6D8</color>
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5.  [QmuiDemo.git](https://github.com/XiaoXiaoXian/QmuiDemo.git)
6.  [Android LoginUI](https://github.com/Shashank02051997/LoginUI-Android.git)
7.  [android_login_demo](https://gitee.com/xrz_admin/android_login_demo.git)
8.  [登录页面3D 旋转](https://gitee.com/xrz_admin/threeDLogin.git)
9.  [android+扫描登录后台](https://gitee.com/sagi/QRCodeLogin.git)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
